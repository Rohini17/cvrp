from __future__ import print_function
from six.moves import xrange
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2
import sys
import json
from datetime import datetime
import math 
import traceback




import logging

class Vehicle():
    """Stores the property of a vehicle"""

    def __init__(self, data):
        """Initializes the vehicle properties"""
        self._capacity = data['capacity']
        self._speed = data['speed'] * 60 / 3.6  # Travel speed: 20km/h to convert in m/min

    @property
    def capacity(self):
        """Gets vehicle capacity"""
        return self._capacity

    @property
    def speed(self):
        """Gets the average travel speed of a vehicle"""
        return self._speed

class DataProblem():
    """Stores the data for the problem"""
    def __init__(self, data):
        """Initializes the data for the problem"""
        self._vehicle = Vehicle(data)
        self._num_vehicles =data['num_vehicles']
        self._locations = data['locations']
        self._names = data['names']
        self._depot = data['depot']
        self._demands = data['demands']
        self.buffer_time_at_each_location = data['buffer_time_at_each_location']
        self.waiting_time = data['waiting_time']
        self.deliver_by_time = data['deliver_by_time']
        self.max_distance = data['max_distance'] 

    @property
    def vehicle(self):
        """Gets a vehicle"""
        return self._vehicle

    @property
    def num_vehicles(self):
        """Gets number of vehicles"""
        return self._num_vehicles

    @property
    def locations(self):
        """Gets locations"""
        return self._locations

    @property
    def num_locations(self):
        """Gets number of locations"""
        return len(self.locations)

    @property
    def depot(self):
        """Gets depot location index"""
        return self._depot

    @property
    def demands(self):
        """Gets demands at each location"""
        return self._demands

    @property
    def buffer_time_at_each_location(self):
        """Gets the time (in min) to load a demand"""
        return self.buffer_time_at_each_location  # 2 minutes/unit

    @property
    def waiting_time(self):
        return self.waiting_time  # 0 min

    @property
    def deliver_by_time(self):
        return self.deliver_by_time  # 0 min

    @property
    def max_distance(self):
        return self.max_distance # km - ( (km/60) - ( deliver_by_time - time_per_demand_unit  ) )  
    

  
# Problem Constraints #

def manhattan_distance(locations,max_distance , position_1, position_2):

    lat_1 = float(position_1.split(",")[0]) 
    lng_1 = float(position_1.split(",")[1])
    lat_2 = float(position_2.split(",")[0])
    lng_2 = float(position_2.split(",")[1])
    R = 6371; 
    x1 = lat_2-lat_1
    dLat = x1 * math.pi / 180
    x2 = lng_2-lng_1
    dLon = x2 * math.pi / 180

    latitude_distance_a =  math.sin(dLat/2) * math.sin(dLat/2)  
    latitude_distance_b = 2 * math.atan2(math.sqrt(latitude_distance_a), math.sqrt(1-latitude_distance_a))
    latitude_distance =  R * latitude_distance_b

    longitude_distance_a =    math.sin(dLon/2) * math.sin(dLon/2)
    longitude_distance_b = 2 * math.atan2(math.sqrt(longitude_distance_a), math.sqrt(1-longitude_distance_a))
    longitude_distance =  R * longitude_distance_b
    # print(( (abs(latitude_distance ) + abs(longitude_distance))  * 1000 ) )

    dist_output = ( (abs(latitude_distance ) + abs(longitude_distance)) )  

    if( dist_output >= max_distance and locations[0] == position_1 ) : 
        return (  max_distance * 1000 ) 
    else : 
        return ( (abs(latitude_distance ) + abs(longitude_distance))  * 1000 ) 
   

class CreateDistanceEvaluator(object):
    """Creates callback to return distance between points."""

    def __init__(self, data):
        """Initializes the distance matrix."""
        self._distances = {}
        for from_node in xrange(data.num_locations):
            self._distances[from_node] = {}
            for to_node in xrange(data.num_locations):
                if from_node == to_node:
                    self._distances[from_node][to_node] = 0
                else:
                    self._distances[from_node][to_node] = (
                        manhattan_distance(
                            data.locations,
                            data.max_distance ,
                            data.locations[from_node],
                            data.locations[to_node]))
                    
    def distance_evaluator(self, from_node, to_node):
        """Returns the google distance matrix distance between the two nodes"""
        return self._distances[from_node][to_node]

# def add_distance_dimension(routing, dist_callback):
#   """Add Global Span constraint"""
#   distance = "Distance"
#   maximum_distance = 2000
#   routing.AddDimension(
#     dist_callback,
#     0, # null slack
#     maximum_distance, # maximum distance per vehicle
#     True, # start cumul to zero
#     distance)
#   distance_dimension = routing.GetDimensionOrDie(distance)
#   # Try to minimize the max distance among vehicles.
#   distance_dimension.SetGlobalSpanCostCoefficient(100)

class CreateDemandEvaluator(object):
    """Creates callback to get demands at each location."""

    def __init__(self, data):
        """Initializes the demand array."""
        self._demands = data.demands

    def demand_evaluator(self, from_node, to_node):
        """Returns the demand of the current node"""
        del to_node
        return self._demands[from_node]


def add_capacity_constraints(routing, data, demand_evaluator):
    """Adds capacity constraint"""
    capacity = "Capacity"
    routing.AddDimension(
        demand_evaluator,
        0, 
        data.vehicle.capacity,  
        True,  
        capacity)


class CreateTimeEvaluator(object):
    """Creates callback to get total times between locations."""
    @staticmethod
    def service_time(data, node):
        """Gets the service time for the specified location."""
        return data.buffer_time_at_each_location 

    @staticmethod
    def travel_time(data, from_node, to_node):
        """Gets the travel times between two locations."""
        if from_node == to_node:
            travel_time = 0
        else:
            travel_time = manhattan_distance(
                data.locations,
                data.max_distance, 
                data.locations[from_node],
                data.locations[to_node]) / data.vehicle.speed
        return travel_time

    def __init__(self, data):
        """Initializes the total time matrix."""
        self._total_time = {}
        for from_node in xrange(data.num_locations):
            self._total_time[from_node] = {}
            for to_node in xrange(data.num_locations):
                if from_node == to_node:
                    self._total_time[from_node][to_node] = 0
                elif to_node == 0 :
                    self._total_time[from_node][to_node] = 0
                else:
                    self._total_time[from_node][to_node] = int(
                        self.service_time(data, from_node) +
                        self.travel_time(data, from_node, to_node))
            

    def time_evaluator(self, from_node, to_node):
        """Returns the total time between the two nodes"""
        return self._total_time[from_node][to_node]


def add_time_window_constraints(routing, data, time_evaluator):
    """Add Global Span constraint"""
    time = "Time"
    waiting_time = data.waiting_time         
    deliver_by_time = data.deliver_by_time           
    routing.AddDimension(
        time_evaluator,
        waiting_time,  # allow waiting time
        deliver_by_time,  # maximum time per vehicle
        True ,  # don't force start cumul to zero since we are giving TW to start nodes
        time)
    time_dimension = routing.GetDimensionOrDie(time)

    time_dimension.SetSpanCostCoefficientForVehicle(30, 10)
    time_dimension.SetSpanUpperBoundForVehicle(30, 10)
# Printer #


class ConsolePrinter():
    """Print solution to console"""

    def __init__(self, data, routing, assignment):
        """Initializes the printer"""
        self._data = data
        self._routing = routing
        self._assignment = assignment

    @property
    def data(self):
        """Gets problem data"""
        return self._data

    @property
    def routing(self):
        """Gets routing model"""
        return self._routing

    @property
    def assignment(self):
        """Gets routing model"""
        return self._assignment
    def print(self):
        vehiclesRequired = 0
        """Prints assignment on console"""
        # Inspect solution.
        capacity_dimension = self.routing.GetDimensionOrDie('Capacity')
        time_dimension = self.routing.GetDimensionOrDie('Time')
        total_dist = 0
        total_time = 0
        final_output = { }
        final_output['order_info'] = [ ] 
        # final_output['order_info']['no_of_deliveries'] = 0 
        for vehicle_id in xrange(self.data.num_vehicles):
            index = self.routing.Start(vehicle_id)
            json_output = {}
            json_output['vehicle_id'] = vehicle_id + 1
            route_dist = 0
            json_output['route_info'] = [] 
            while not self.routing.IsEnd(index):
                node_index = self.routing.IndexToNode(index)
                next_node_index = self.routing.IndexToNode(self.assignment.Value(self.routing.NextVar(index)))
                if next_node_index != 0 :
                    route_dist += manhattan_distance(
                        self.data.locations,
                        self.data.max_distance, 
                        self.data.locations[node_index],
                        self.data.locations[next_node_index])
                load_var = capacity_dimension.CumulVar(index)
                route_load = self.data.vehicle.capacity - self.assignment.Value(load_var)
                time_var = time_dimension.CumulVar(index)
                time_min = self.assignment.Min(time_var)
                time_max = self.assignment.Max(time_var)
                route_json = {}
                route_json['coordinates'] =  '{0}'.format(self.data.locations[node_index] )
                route_json['distance'] =  '{0}'.format(route_dist )
                route_json['node_index'] =  '{0}'.format(node_index)
                route_json['route_load'] =  '{0}'.format(route_load)
                route_json['time_min']   =  '{0}'.format(time_min)
                route_json['time_max'] =  '{0}'.format(time_max)
                route_json['name'] =  '{0}'.format(self.data._names[node_index])
                json_output['route_info'].append(route_json)
                # final_output['order_info']['no_of_deliveries'] += 1  
                index = self.assignment.Value(self.routing.NextVar(index))
            node_index = self.routing.IndexToNode(index)
            load_var = capacity_dimension.CumulVar(index)
            route_load = self.data.vehicle.capacity - self.assignment.Value(load_var)

            
            time_var = time_dimension.CumulVar(index)
            route_time = self.assignment.Value(time_var)
            time_min = self.assignment.Min(time_var)
            time_max = self.assignment.Max(time_var)      
            
            total_dist += route_dist
            total_time += route_time
            json_output['distance'] =  '{0}'.format(route_dist)
            json_output['load'] = format(self.assignment.Value(load_var)) 
            load_output = json_output['load']  
            json_output['time'] =   '{0}'.format(route_time) 
            if not(route_dist <= 0 and route_time <= 0   ):
                vehiclesRequired = vehiclesRequired + 1
            if ( int(load_output) != 0 ) :
                final_output['order_info'].append(json_output)
            
        final_output['distance'] = '{0}'.format(total_dist)
        final_output['vehiclesRequired'] = '{0}'.format(vehiclesRequired)
        final_output['time'] =   '{0}'.format(total_time) 
        
        print(final_output)
    

# Main #


def main():
    
    """Entry point of the program"""
    data1 = sys.argv[1] 
    o = json.loads(data1)

    # Instantiate the data problem.
    data = DataProblem(o)

    # Create Routing Model
    routing = pywrapcp.RoutingModel(data.num_locations, data.num_vehicles, data.depot)
    # Define weight of each edge
    distance_evaluator = CreateDistanceEvaluator(data).distance_evaluator
    # add_distance_dimension(routing, distance_evaluator)

    routing.SetArcCostEvaluatorOfAllVehicles(distance_evaluator)
    # Add Capacity constraint
    demand_evaluator = CreateDemandEvaluator(data).demand_evaluator
    add_capacity_constraints(routing, data, demand_evaluator)
    # Add Time Window constraint
    time_evaluator = CreateTimeEvaluator(data).time_evaluator
    add_time_window_constraints(routing, data, time_evaluator)

    # Setting first solution heuristic (cheapest addition).
    search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION)
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    search_parameters.time_limit_ms = 50000

    # Solve the problem.
    assignment = routing.SolveWithParameters(search_parameters)

    if assignment is None:
        err_data = {}
        err_data['err'] = 'No Solution Found!'
        print(err_data )
    else:
        printer = ConsolePrinter(data, routing, assignment)
        printer.print()
  


if __name__ == '__main__':
    main()
