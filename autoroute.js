

var express     =   require('express');
var app         =   express();
var fs          =   require("fs");
var bodyParser  =   require('body-parser');



app.use(bodyParser.json({limit: '50mb'}));

app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));

app.post('/autorouting', function (req, res, next ) {
  var dataString = ''; 
  var spawn = require("child_process").spawn; 
  var process = spawn('python', ["autoroute.py", JSON.stringify(req.body) ]);
  process.stdout.on("data", function (data) {
    try{
      dataString += data.toString().replace(/\'/g,  '"') 
    }catch(e){
      console.log( 'Parsing Error : ' , dataString  )
    }
  });
  process.stderr.on('data', function(data) {
    return res.json({ status_code : 500 , status : 'failure' , message : 'Error in getting python output' , err : data.toString() })
  });
  process.stdout.on('end', function(){
    var data =  JSON.parse(dataString) 
    console.log(typeof data  )
    return res.json({  status_code : 200 , statud : 'success' , message : 'Output has been received sucessfully!' , data :data })
  });
  process.stdin.write(JSON.stringify(req.body));
  process.stdin.end();
})

var server = app.listen(8081, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at http://%s:%s", host, port)
})
